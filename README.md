# Pyva

Pyva is a Java library that allows invoking global Python functions and
passing Java objects to them as parameters.

These Java objects are wrapped by a native 'Pyva' Python object. These objects
can be treated as normal Python objects; all __getattr__ and __setattr__
operations on them instead access the underlying Java object.

Once a Pyva object is created it is held for the lifetime of the underlying
Java object it is associated with. Passing the same Java object to Python
through Pyva will always result in the same Pyva object.

Holding a reference to a Pyva object will properly prevent the underlying JVM
from garbage collecting the associated Java object.

Furthermore, Pyva objects implement __getitem__ and __setitem__ (as well as
__contains__), so you can access the underlying Java object's attributes via
the dictionary syntax, as well as set new attributes on Pyva objects that do
not exist on the underlying Java objects.


To compile:

```
cd pyva-native
cp make.example.properties make.properties
$EDITOR make.properties
```

Then:
```
mvn install
```


Example test.py:

```
def myfunc(string):
	print string
	return 42
```


Example test.java:

```
Pyva pyva = new Pyva();
pyva.addToSystemPath(".");
pyva.init();
Integer i = (Integer) pyva.invoke("test", "myfunc", "hello!");
```

You may have to place libpython.so in LD_PRELOAD for the JVM to be able to load it.


Java classes may also be imported via Pyva to Python, for example:

import pyva_java_util_Date as Date

Which would allow you to construct a native Java 'Date' via Date(42L) in Python.
