#include "python.hpp"
#include "pyva_object.hpp"

PyMODINIT_FUNC
initlibpyva(void)
{
	PyObject *m = Py_InitModule("libpyva", nullptr);

	if (m == nullptr)
		return;

	/* We can only be imported from within pyva, not directly */
	if (!(PyvaObjectType.tp_flags & Py_TPFLAGS_READY) || !(PyvaExceptionType.tp_flags & Py_TPFLAGS_READY))
	{
		PyErr_SetString(PyExc_RuntimeError, "libpyva may not be imported directly");
		return;
	}

	Py_INCREF(&PyvaExceptionType);
	if (PyModule_AddObject(m, "PyvaException", reinterpret_cast<PyObject *>(&PyvaExceptionType)) < 0)
		return;
}

