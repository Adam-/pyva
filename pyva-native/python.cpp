#include "python.hpp"

PyThreadState *Python::state = nullptr;

void Python::Start()
{
	assert(Py_IsInitialized() == false);
	assert(state == nullptr);

	Py_Initialize();

	/* threads are initialized once per process, not per interpreter, so this
	 * call does nothing if we are reloading pyva
	 */
	PyEval_InitThreads();

	/* Gets the thread state of the main interpreter, which is what we use to create the sub interpreters,
	 * which is where most of our code is run.
	 */
	state = PyEval_SaveThread();
}

void Python::Stop()
{
	assert(state != nullptr);

	/* Delete all sub interpreters, each destructor will context switch to each interpreter's thread state
	 * and properly clean up, releasing the GIL
	 */
	Thread::Lock interpreter_lock(Pyva::interpreters_lock);
	for (std::vector<Pyva *>::iterator it = Pyva::interpreters.begin(); it != Pyva::interpreters.end();)
	{
		Pyva *p = *it;
		it = Pyva::interpreters.erase(it);
		delete p;
	}

	/* Now hold the the main interpreter lock */
	Python::Lock lock;

	/* shutdown the main interpreter */
	Py_Finalize();

	state = nullptr;
}

template<typename T>
Python::TypedReference<T>::TypedReference() : obj(nullptr)
{
}

template<typename T>
Python::TypedReference<T>::TypedReference(T *o) : obj(o)
{
}

template<typename T>
Python::TypedReference<T>::TypedReference(const TypedReference<T> &other)
{
	obj = other.obj;
	Py_XINCREF(obj);
}

template<typename T>
Python::TypedReference<T>::~TypedReference()
{
	Py_XDECREF(obj);
}

template<typename T>
Python::TypedReference<T>& Python::TypedReference<T>::operator=(const TypedReference<T> &other)
{
	if (this != &other)
	{
		Py_XDECREF(obj);
		obj = other.obj;
		Py_XINCREF(obj);
	}
	return *this;
}

template<typename T>
Python::TypedReference<T>::operator T*() const
{
	return obj;
}

template<typename T>
T* Python::TypedReference<T>::operator->() const
{
	return obj;
}

template<typename T>
T** Python::TypedReference<T>::Pointer()
{
	return &obj;
}

template class Python::TypedReference<PyObject>;
template class Python::TypedReference<PyCodeObject>;
template class Python::TypedReference<PyFrameObject>;

Python::Lock::Lock()
{
	PyEval_RestoreThread(state);
}

Python::Lock::Lock(Pyva *pyva)
{
	PyEval_RestoreThread(pyva->state);
}

Python::Lock::~Lock()
{
	if (!Py_IsInitialized())
		return; /* interpreter destroyed out from under us */

	PyEval_SaveThread();
}

Python::AllowThreads::AllowThreads()
{
	save = PyEval_SaveThread();
}

Python::AllowThreads::~AllowThreads()
{
	PyEval_RestoreThread(save);
}

