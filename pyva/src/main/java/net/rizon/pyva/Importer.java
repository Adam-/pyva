package net.rizon.pyva;

/* An instance of this class is passed to pyva.py, and is used by it to import Pyva references to Java classes */
public class Importer
{
	private Class<?> pyva_import(String name)
	{
		try
		{
			return Class.forName(name);
		}
		catch (ClassNotFoundException ex)
		{
			return null;
		}
	}
}

