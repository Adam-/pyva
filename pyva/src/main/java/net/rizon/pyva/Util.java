package net.rizon.pyva;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Iterator;

/* Utility functions that are called from native code because it's easier to implement them in Java than in C++ */
public class Util
{
	private static boolean isArray(Object obj)
	{
		return obj.getClass().isArray();
	}

	private static Iterator<?> getIterator(Object object)
	{
		if (object instanceof Iterable)
			return ((Iterable) object).iterator();
		return null;
	}

	private static boolean hasNextIterator(Iterator<?> it)
	{
		return it.hasNext();
	}

	private static Object nextIterator(Iterator<?> it)
	{
		return it.next();
	}

	/* unbox classes for looping up methods, this means calling methods with boxed primitive
	 * types isn't supported
	 */
	private static Class<?> getPrimitiveClass(Class<?> c)
	{
		if (c == Boolean.class)
			return boolean.class;
		else if (c == Character.class)
			return char.class;
		else if (c == Byte.class)
			return byte.class;
		else if (c == Short.class)
			return short.class;
		else if (c == Integer.class)
			return int.class;
		else if (c == Long.class)
			return long.class;
		else if (c == Float.class)
			return float.class;
		else if (c == Double.class)
			return double.class;
		else if (c == Void.class)
			return void.class;
		else
			return c;
	}

	private static Class<?> getClass(Object object)
	{
		/* Gets the class of the object, unless the object is a class, then it just returns the object */
		return object instanceof Class ? (Class<?>) object : object.getClass();
	}

	private static String getClassName(Object object)
	{
		return getClass(object).getName();
	}

	private static Object getAttribute(Pyva pyva, Object object, String name)
	{
		for (Class<?> cl = getClass(object); cl != null; cl = cl.getSuperclass())
		{
			/* We can't getDeclaredMethod becauase we don't know the arguments, so instead just see if there is one method that matches */
			for (Method m : cl.getDeclaredMethods())
				if (m.getName().equals(name))
					return m;

			try
			{
				Field f = cl.getDeclaredField(name);
				f.setAccessible(true); // XXX
				return f.get(object);
			}
			catch (NoSuchFieldException ex)
			{
			}
			catch (IllegalAccessException ex)
			{
			}
		}

		/* this is okay, we can return null to fall back to Python's default get attribute behavior */
		return null;
	}

	private static boolean setAttribute(Pyva pyva, Object object, String name, Object value)
	{
		Field f = null;

		for (Class<?> cl = getClass(object); cl != null; cl = cl.getSuperclass())
		{
			try
			{
				f = cl.getDeclaredField(name);
			}
			catch (NoSuchFieldException ex)
			{
			}
		}

		/* this is okay, we can return false and fall back to Python's default set attribute behavior */
		if (f == null)
			return false;

		try
		{
			f.set(object, value);
		}
		catch (Exception ex)
		{
			System.err.println("Exception while setting attribute " + name + " to " + value + " on " + object);
			ex.printStackTrace(); // XXX
		}

		return true;
	}

	private static Object invokeConstructor(Pyva pyva, Class<?> clazz, Object... args) throws NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException
	{
		/* Get the classes for the arguments */
		Class<?> classes[] = new Class[args.length];
		for (int i = 0; i < args.length; ++i)
			classes[i] = getPrimitiveClass(args[i].getClass());

		Constructor<?> con = clazz.getDeclaredConstructor(classes);
		return con.newInstance(args);
	}

	private static Object invoke(Pyva pyva, Method method, Object object, Object... args) throws IllegalAccessException, InvocationTargetException
	{
		/* The method 'method' is the method identified by getAttribute, which may not be the correct method as Java
		 * allows overloading. Now that we have the arguments, try and identify the correct method.
		 */

		/* Get the classes for the arguments */
		Class<?> classes[] = new Class[args.length];
		for (int i = 0; i < args.length; ++i)
			classes[i] = getPrimitiveClass(args[i].getClass());

		for (Class<?> cl = getClass(object); cl != null; cl = cl.getSuperclass())
		{
			/* Look up new method based on the arguments */
			try
			{
				method = cl.getDeclaredMethod(method.getName(), classes);
				break;
			}
			catch (NoSuchMethodException ex)
			{
			}
		}

		method.setAccessible(true); // XXX
		return method.invoke(object, args);
	}

	private static Throwable getCause(Throwable ex)
	{
		while (ex.getCause() != null)
			ex = ex.getCause();
		return ex;
	}

	private static Object[][] splitException(Throwable ex)
	{
		StackTraceElement[] stackTrace = ex.getStackTrace();
		Object[][] obj = new Object[stackTrace.length][];
		int i = 0;

		for (StackTraceElement ste : ex.getStackTrace())
		{
			Object[] info = new Object[4];

			info[0] = ste.getClassName() != null ? ste.getClassName() : "<unknown>";
			info[1] = ste.getMethodName() != null ? ste.getMethodName() : "<unknown>";
			info[2] = ste.getFileName() != null ? ste.getFileName() : "<unknown>";
			info[3] = ste.getLineNumber();

			obj[i++] = info;
		}

		return obj;
	}

	private static void prettyException(Throwable ex)
	{
		/* remove native frames */
		ArrayList<StackTraceElement> trace = new ArrayList<StackTraceElement>(Arrays.asList(ex.getStackTrace()));
		boolean inNative = false;

		for (Iterator<StackTraceElement> it = trace.iterator(); it.hasNext();)
		{
			StackTraceElement e = it.next();

			if (e.getLineNumber() == -2)
			{
				inNative = !inNative;
				it.remove();
				continue;
			}

			if (inNative)
				it.remove();
		}

		StackTraceElement[] ste = new StackTraceElement[trace.size()];
		trace.toArray(ste);
		ex.setStackTrace(ste);
	}
}

