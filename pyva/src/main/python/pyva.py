import sys

class PyvaImporter(object):
	def __init__(self, pyva):
		self.pyva = pyva
	
	def find_module(self, name, path):
		# We're only interested in pyva_ modules
		if not name.startswith('pyva_'):
			return None

		# pyva_java_lang_System -> java.lang.System
		name = name[5:].replace('_', '.')

		if self.pyva.pyva_import(name):
			# We act as the loader, too
			return self
		else:
			return None

	def load_module(self, name):
		if name in sys.modules:
			return sys.modules[name]

		name = name[5:].replace('_', '.')
		return self.pyva.pyva_import(name)

def init(pyva):
	for loader in sys.meta_path:
		if isinstance(loader, PyvaImporter):
			return

	sys.meta_path.append(PyvaImporter(pyva))

def get_stacktrace(traceback):
	st = []
	frame = traceback
	while frame:
		st.append(frame)
		frame = frame.tb_next
	return st

def invoke(module, method, args):
	try:
		pymod = __import__(module)
		pyfunc = getattr(pymod, method)
		return pyfunc(*args)
	except:
		# Catch all exceptions, get exception info then wrap the exception in a net.rizon.util.PyvaException and throw that
		type, value, traceback = sys.exc_info()

		# We can't import using pyva_ until after init has been called to init the importer
		import pyva_net_rizon_pyva_PyvaException as PyvaException
		from libpyva import PyvaException as NativePyvaException
		import pyva_java_lang_StackTraceElement as StackTraceElement

		# Create a new Java exception
		ex = PyvaException(str(value))
		newStackTrace = []

		if isinstance(value, NativePyvaException):
			# If the value being thrown is a pyva object (thus has an underlying Java exception), we can set the cause to that
			ex.initCause(value)

			# Since we have an underlying Java stacktrace, append that until we reach into the native code, instead of using our
			# generated Python frames, which has less information
			for trace in value.getStackTrace():
				if trace.getLineNumber() == -2:
					break

				newStackTrace.append(trace)

			# traceback will include the Java frames, then our frames, then whatever is above us, which is more Java unless
			# some rogue python code is directly invoking this function, so, skip until we find Python frames and then append
			# those until Java frames are found
			seen = False
			for frame in reversed(get_stacktrace(traceback)):
				if frame.tb_frame.f_code.co_filename.endswith('.java'):
					if seen:
						break
					continue

				seen = True
				ste = StackTraceElement("pyva", frame.tb_frame.f_code.co_name, frame.tb_frame.f_code.co_filename, frame.tb_lineno)
				newStackTrace.append(ste)
		else:
			# Iterate over frames and add them to the exception
			for frame in reversed(get_stacktrace(traceback)):
				ste = StackTraceElement("pyva", frame.tb_frame.f_code.co_name, frame.tb_frame.f_code.co_filename, frame.tb_lineno)
				newStackTrace.append(ste)

		# Append rest of backtrace, this is Java-only though so pervious Python frames won't be here?
		for trace in ex.getStackTrace():
			newStackTrace.append(trace)

		# Attach new backtrace
		ex.setStackTrace(newStackTrace)

		# Raise the new exception
		raise ex
